﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JuiceIt : MonoBehaviour {

    private AudioSource source;

	// Use this for initialization
	void Start () {
        source = GetComponent<AudioSource>();
        StartCoroutine(TELLIT());
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    IEnumerator TELLIT()
    {
        yield return new WaitForSeconds(4.0f);
        source.Play();
    }
}
