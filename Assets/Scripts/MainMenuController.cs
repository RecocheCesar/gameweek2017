﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class MainMenuController : MonoBehaviour {


    private AudioSource source;

    [SerializeField]
    private AudioClip clip;

    [SerializeField]
    private GameObject eventSystem;
    [SerializeField]
    private GameObject playa;

	// Use this for initialization
	void Start ()
    {
        source = GetComponent<AudioSource>();
	}
	
	// Update is called once per frame
	void Update ()
    {
		if(!source.isPlaying)
        {
            source.clip = clip;
            source.Play();
        }
	}

    public void ChangeCanvas(GameObject go)
    {
        go.transform.GetChild(1).gameObject.SetActive(false);
        go.transform.GetChild(2).gameObject.SetActive(true);
   
        eventSystem.GetComponent<EventSystem>().SetSelectedGameObject(playa);
    }
}
