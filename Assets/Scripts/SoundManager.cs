﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour
{

    [SerializeField]
    private AudioClip[] MusiquesMode;

    private AudioSource source;

    // Use this for initialization
    void Start ()
    {
        source = GetComponent<AudioSource>();
	}
	
	// Update is called once per frame
	void Update ()
    {
		if(source.clip != MusiquesMode[0] && PlayersData.GetInstance().mode == Mode.Construction)
        {
            source.clip = MusiquesMode[0];
            source.Play();
        }
        if(source.clip != MusiquesMode[1] && PlayersData.GetInstance().mode == Mode.Jeu)
        {
            source.Stop();
            source.clip = MusiquesMode[1];
            source.Play();
        }
	}
}
