﻿using UnityEngine;
using System.Collections;


public enum Mode { Construction, Jeu, Score };

public class ControllerPlayer : MonoBehaviour
{
   
    public int NbPlayer = 0;

    [SerializeField]
    private float speed = 400;

    private Rigidbody2D rb;

    [SerializeField]
    private Transform[] restZone;

    Vector2 axis;
   
    // Use this for initialization
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        axis = Vector2.zero;
    }

    // Update is called once per frame
    void Update()
    {
        if (PlayersData.GetInstance().mode == Mode.Construction)
        {
            axis.x = Input.GetAxis("LeftJoystickX_P" + NbPlayer);
            axis.y = -Input.GetAxis("LeftJoystickY_P" + NbPlayer);
            axis.Normalize();

            if (PlayersData.GetInstance().nbNotes[NbPlayer - 1] == 0)
            {
                transform.position = restZone[NbPlayer - 1].position;
            }

        }
    }

    void FixedUpdate()
    {
        if (PlayersData.GetInstance().mode == Mode.Construction)
        {
            rb.velocity = axis * Time.fixedDeltaTime * speed;
        }
    }
}
