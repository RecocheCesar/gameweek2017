﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PutButton : MonoBehaviour
{

    [SerializeField]
    private Sprite[] buttons;

    private SpriteRenderer image;

    private bool occupied = false;

    private AudioSource source;

    // Use this for initialization
    void Start()
    {
        image = GetComponent<SpriteRenderer>();
        source = GetComponent<AudioSource>();
    }


    void OnTriggerEnter2D(Collider2D col)
    {
        if (!occupied && col.gameObject.tag == "Player" || !occupied && col.gameObject.tag == "Player2")
            image.color = new Color(image.color.r, image.color.g, image.color.b, 0.5f);
    }

    void OnTriggerExit2D(Collider2D col)
    {
        if (!occupied && col.gameObject.tag == "Player" || !occupied && col.gameObject.tag == "Player2")
            image.color = new Color(image.color.r, image.color.g, image.color.b, 1.0f);
    }

    void OnTriggerStay2D(Collider2D col)
    {
        if (!occupied && col.gameObject.tag == "Player")
        {
            if (Input.GetButtonDown("A_P1"))
                PutImage(buttons[0], col.gameObject);

            else if (Input.GetButtonDown("B_P1"))
                PutImage(buttons[1], col.gameObject);

            else if (Input.GetButtonDown("X_P1"))
                PutImage(buttons[2], col.gameObject);

            else if (Input.GetButtonDown("Y_P1"))
                PutImage(buttons[3], col.gameObject);
        }

        else if (!occupied && col.gameObject.tag == "Player2")
        {
            if (Input.GetButtonDown("A_P2"))
                PutImage(buttons[0], col.gameObject);

            else if (Input.GetButtonDown("B_P2"))
                PutImage(buttons[1], col.gameObject);

            else if (Input.GetButtonDown("X_P2"))
                PutImage(buttons[2], col.gameObject);

            else if (Input.GetButtonDown("Y_P2"))
                PutImage(buttons[3], col.gameObject);
        }
    }

    void PutImage(Sprite sp, GameObject go)
    {
        image.sprite = sp;
        occupied = true;
        image.color = new Color(image.color.r, image.color.g, image.color.b, 1.0f);
        PlayersData.GetInstance().nbNotes[go.GetComponent<ControllerPlayer>().NbPlayer - 1]--;
        source.Play();
    }
}
