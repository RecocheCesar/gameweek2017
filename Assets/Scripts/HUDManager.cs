﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HUDManager : MonoBehaviour
{

    [SerializeField]
    private TextMesh[] hud;

    [SerializeField]
    private Text[] scores;

	// Use this for initialization
	void Start ()
    {

    }

    // Update is called once per frame
    void Update ()
    {
        hud[0].text = PlayersData.GetInstance().nbNotes[0].ToString();
        hud[1].text = PlayersData.GetInstance().nbNotes[1].ToString();

        scores[0].text = PlayersData.GetInstance().scores[0].ToString();
        scores[1].text = PlayersData.GetInstance().scores[1].ToString();

    }
}
