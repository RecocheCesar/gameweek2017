﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayGame : MonoBehaviour
{
    [SerializeField]
    private Transform slots;
    private List<Vector3> basePositionSlots = new List<Vector3>();

    [SerializeField]
    private ControllerPlayer cpScript;

    [SerializeField]
    private GameObject[] ObjectConstruction;

    [SerializeField]
    private Transform[] posPart;

    [SerializeField]
    private Transform posConstruction;
    [SerializeField]
    private GameObject Partition1;

    private GameObject Partition2;

    [SerializeField]
    private GameObject scoreBoard;

    private AudioSource source;

    void Start()
    {
        source = GetComponent<AudioSource>();
        for (int i = 0; i < slots.childCount; ++i)
        {
            basePositionSlots.Add(slots.GetChild(i).localPosition);
        }
    }

    void Update()
    {
        if (PlayersData.GetInstance().nbNotes[0] == 0 && PlayersData.GetInstance().nbNotes[1] == 0 && PlayersData.GetInstance().mode == Mode.Construction)
            StartCoroutine(TimeToLearn());
    }

    public void LaunchPartition()
    {
        PlayersData.GetInstance().mode = Mode.Jeu;

        slots.position = new Vector3(slots.position.x + 16, slots.position.y, slots.position.z);
        for (int i = 0; i < slots.childCount; ++i)
        {
            slots.GetChild(i).GetComponent<Defilement>().enabled = true;

            if (slots.GetChild(i).GetComponent<SpriteRenderer>().sprite.name != "R")
            {
                slots.GetChild(i).GetChild(0).gameObject.SetActive(true);
            }
            else
            {
                slots.GetChild(i).gameObject.GetComponent<Renderer>().material.color = new Color(1, 1, 1, 0.5f);
            }
        }


        Partition1.transform.position = posPart[0].position;
        Partition2 = GameObject.Instantiate(Partition1, posPart[1].position, Quaternion.identity);
        Partition2.transform.GetChild(0).GetComponent<PressScript>().playerLinked = 2;

        Partition2.transform.GetChild(0).GetComponent<Renderer>().material.color = Color.green;
        //ICICIIFIEIFOEFREOHGREG
    }

    IEnumerator TimeToLearn()
    {
        source.Play();
        PlayersData.GetInstance().mode = Mode.Score;
        yield return new WaitForSeconds(2.0f);
        PlayersData.GetInstance().mode = Mode.Jeu;

        LaunchPartition();
    }

    public void ResetConstruction()
    {
        PlayersData.GetInstance().nbNotes[0] = 1;
        PlayersData.GetInstance().nbNotes[1] = 1;

        PlayersData.GetInstance().mode = Mode.Construction;
        PlayersData.GetInstance().round++;

        Partition1.transform.position = posConstruction.position;

        for (int i = 0; i < slots.childCount; ++i)
        {
            slots.GetChild(i).GetComponent<Defilement>().enabled = false;
            slots.GetChild(i).GetChild(0).GetComponent<Renderer>().material.color = Color.white;

            if (slots.GetChild(i).GetComponent<SpriteRenderer>().sprite.name != "R")
            {
                slots.GetChild(i).GetChild(0).gameObject.SetActive(false);
            }
            else
            {
                slots.GetChild(i).gameObject.GetComponent<Renderer>().material.color = new Color(1, 1, 1, 1);
            }

            slots.GetChild(i).localPosition = basePositionSlots[i];
        }

        slots.position = new Vector3(0, slots.position.y, slots.position.z);
        Destroy(Partition2.gameObject);

        ObjectConstruction[0].GetComponent<Renderer>().enabled = true;
        ObjectConstruction[1].GetComponent<Renderer>().enabled = true;

        if (PlayersData.GetInstance().round == 5)
        {
            PlayersData.GetInstance().mode = Mode.Score;
            StartCoroutine(DisplayScore());
        }
    }

    IEnumerator DisplayScore()
    {
        PlayersData.GetInstance().gameObject.GetComponent<AudioSource>().Play();
        yield return new WaitForSeconds(2.0f);
        scoreBoard.SetActive(true);
        if (PlayersData.GetInstance().scores[0] >= PlayersData.GetInstance().scores[1])
            scoreBoard.transform.GetChild(1).GetComponent<Text>().text = "1";
        else
            scoreBoard.transform.GetChild(1).GetComponent<Text>().text = "2";


        yield return new WaitForSeconds(3.0f);

        Application.LoadLevel(1);

    }
}
