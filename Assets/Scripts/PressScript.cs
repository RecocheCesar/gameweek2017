﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PressScript : MonoBehaviour
{
    private int current = -1;

    private Transform currentGameObject;

    public int playerLinked;

    private int malus = 23;

    [SerializeField]
    private AudioClip[] clip;
    private AudioSource source;

    private Animator anim;


    void Start()
    {
        source = GetComponent<AudioSource>();
        anim = GetComponent<Animator>();
        StartCoroutine(AnimPress());

    }

    void OnTriggerEnter2D(Collider2D col)
    {

        if (col.gameObject.tag == "Slot")
        {
            switch (col.gameObject.GetComponent<SpriteRenderer>().sprite.name)
            {
                case "A": current = 0; break;
                case "B": current = 1; break;
                case "X": current = 2; break;
                case "Y": current = 3; break;
            }

            currentGameObject = col.gameObject.transform;
        }
    }

    void Update()
    {
        if (PlayersData.GetInstance().mode != Mode.Construction && PlayersData.GetInstance().mode != Mode.Score)
        {
            if (Input.GetButtonDown("A_P" + playerLinked))
            {
                StartCoroutine(AnimPress());
                if (current == 0)
                {
                    source.PlayOneShot(clip[0]);

                    PlayersData.GetInstance().scores[playerLinked - 1] += (int)(1 / Vector2.Distance(transform.position, currentGameObject.position)) * 10;
                    current = -1;
                    currentGameObject.GetChild(0).GetComponent<Renderer>().material.color = Color.green;
                }
                else
                {
                    current = -1;

                    PlayersData.GetInstance().scores[playerLinked - 1] -= malus;
                    if(currentGameObject != null)
                     currentGameObject.GetChild(0).GetComponent<Renderer>().material.color = Color.red;
                }
            }
            if (Input.GetButtonDown("B_P" + playerLinked))
            {
                StartCoroutine(AnimPress());

                if (current == 1)
                {
                    source.PlayOneShot(clip[1]);

                    PlayersData.GetInstance().scores[playerLinked - 1] += (int)(1 / Vector2.Distance(transform.position, currentGameObject.position)) * 10;
                    current = -1;
                    currentGameObject.GetChild(0).GetComponent<Renderer>().material.color = Color.green;
                }
                else
                {
                    current = -1;

                    PlayersData.GetInstance().scores[playerLinked - 1] -= malus;
                    if (currentGameObject != null)
                        currentGameObject.GetChild(0).GetComponent<Renderer>().material.color = Color.red;
                }
            }
            if (Input.GetButtonDown("X_P" + playerLinked))
            {
                StartCoroutine(AnimPress());

                if (current == 2)
                {
                    source.PlayOneShot(clip[1]);

                    PlayersData.GetInstance().scores[playerLinked - 1] += (int)(1 / Vector2.Distance(transform.position, currentGameObject.position)) * 10;
                    current = -1;
                    currentGameObject.GetChild(0).GetComponent<Renderer>().material.color = Color.green;
                }
                else
                {
                    current = -1;

                    PlayersData.GetInstance().scores[playerLinked - 1] -= malus;
                    if (currentGameObject != null)
                        currentGameObject.GetChild(0).GetComponent<Renderer>().material.color = Color.red;
                }
            }
            if (Input.GetButtonDown("Y_P" + playerLinked))
            {
                StartCoroutine(AnimPress());

                if (current == 3)
                {
                    source.PlayOneShot(clip[3]);
                        
                    PlayersData.GetInstance().scores[playerLinked - 1] += (int)(1 / Vector2.Distance(transform.position, currentGameObject.position)) * 10;
                    current = -1;
                    currentGameObject.GetChild(0).GetComponent<Renderer>().material.color = Color.green;
                }
                else
                {
                    current = -1;

                    PlayersData.GetInstance().scores[playerLinked - 1] -= malus;
                    if (currentGameObject != null)
                        currentGameObject.GetChild(0).GetComponent<Renderer>().material.color = Color.red;
                }
            }

        }
    }

    IEnumerator AnimPress()
    {
        anim.SetBool("Press", true);
        yield return null;
        anim.SetBool("Press", false);
    }
}
