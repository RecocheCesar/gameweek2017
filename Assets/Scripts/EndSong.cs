﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndSong : MonoBehaviour
{
    [SerializeField]
    private PlayGame scriptPlayGame;

	// Use this for initialization
	void Start ()
    {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
		
	}

   void OnTriggerEnter2D(Collider2D col)
    {
        if(col.gameObject.name.Contains("Last") && col.gameObject.transform.parent.parent.name == "J1Part")
        {
            scriptPlayGame.ResetConstruction();
        }
    }
}
