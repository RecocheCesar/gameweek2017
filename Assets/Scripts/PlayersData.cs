﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayersData : MonoBehaviour
{
    private static PlayersData _instance;

    public int[] nbNotes = { 2, 2 };

    public int[] scores = { 0, 0 };

    public Mode mode = Mode.Construction;

    public int round = 0;

	// Use this for initialization
	void Awake ()
    {
        if (!_instance)
            _instance = this;

        else
            Destroy(this.gameObject);
    }
	
	// Update is called once per frame
	void Update ()
    {
        if (Input.GetKeyDown(KeyCode.G))
            nbNotes[0]--;
	}

    static public PlayersData GetInstance()
    {
        if (_instance == null)
            _instance = new PlayersData();

        return _instance;
    }
}

