﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Loader : MonoBehaviour
{

    void Start()
    {
        if(Application.loadedLevel == 0)
        {
            Splash();
        }
    }

    void Update()
    {
  
    }

    public void LoadScene(int i)
    {
        SceneManager.LoadScene(i);
    }

    public void ReloadScene()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    public void QuitGame()
    {
        Application.Quit();
    }

    public void Splash()
    {
        StartCoroutine(waiting());
    }

    IEnumerator waiting()
    {
        yield return new WaitForSeconds(3.0f);
        LoadScene(1);
    }

    public void Credits(GameObject go)
    {
        for(int i =0; i< go.transform.GetChild(1).GetChild(0).childCount;++i)
        {
            go.transform.GetChild(1).GetChild(0).GetChild(i).GetComponent<Image>().enabled = false;
        }
        go.transform.GetChild(3).gameObject.SetActive(true);
        StartCoroutine(Creditwait(go));
    }

    IEnumerator Creditwait(GameObject go)
    {
        yield return new WaitForSeconds(3.0f);
        for (int i = 0; i < go.transform.GetChild(1).GetChild(0).childCount; ++i)
        {
            go.transform.GetChild(1).GetChild(0).GetChild(i).GetComponent<Image>().enabled = true;
        }
        go.transform.GetChild(3).gameObject.SetActive(false);
    }
}
